package sderror

import (
	"github.com/Sirupsen/logrus"
	"testing"
)

const projectID = "your-project-id"

func TestBasicError(t *testing.T) {
	hook, err := NewHook(projectId, "test", "1.0")
	if err != nil {
		t.Fatalf("Failed to create error hook: %v", err)
	}
	logrus.AddHook(hook)
	logrus.WithField("label", "value").Error("Here is an error!")
}
