/*
Package sderror ...
*/
package sderror

import (
	"cloud.google.com/go/errors"
	"github.com/Sirupsen/logrus"
	"golang.org/x/net/context"
)

// Hook ...
type Hook struct {
	client *errors.Client
}

// NewHook ...
func NewHook(projectID, serviceName, serviceVersion string) (*Hook, error) {
	client, err := errors.NewClient(context.Background(), projectID, serviceName, serviceVersion, true)
	if err != nil {
		return nil, err
	}
	return &Hook{client: client}, nil
}

// Fire ...
func (hook *Hook) Fire(entry *logrus.Entry) error {
	line, err := entry.String()
	if err != nil {
		return err
	}

	// We only fire on erroneous log entries and each level follows the same
	// workflow.
	hook.client.Report(
		context.Background(),
		nil, // no associated HTTP request object
		line)
	return nil
}

// Levels ...
func (hook *Hook) Levels() []logrus.Level {
	return []logrus.Level{logrus.PanicLevel, logrus.FatalLevel, logrus.ErrorLevel}
}
